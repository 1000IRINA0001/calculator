package calculator;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.image.Image;


public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("app.fxml"));
        primaryStage.getIcons().add(new Image("calculator\\animation\\icon.png"));
        primaryStage.setTitle("Калькулятор");
        primaryStage.setScene(new Scene(root, 280, 400));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}

