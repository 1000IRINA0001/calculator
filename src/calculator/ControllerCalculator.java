package calculator;

import calculator.animation.Shake;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * класс, демонстрирующий работу калькулятора
 *
 * @author  Прокофьева И.А.
 */
@SuppressWarnings("unused")
public class ControllerCalculator {
    private static final String INPUT_ERROR_MESSAGE = "Введены некорректные данные ";
    private static final String DIVISION_ERROR = "На ноль делить нельзя";
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField firstOperand;

    @FXML
    private TextField secondOperand;

    @FXML
    private Button buttonAdd;

    @FXML
    private Button buttonDiv;

    @FXML
    private Button buttonMult;

    @FXML
    private Button buttonSub;

    @FXML
    private Button buttonClear;

    @FXML
    private TextField result;

    /**
     * метод очистки текстовых полей
     * @param event нажатие кнопки "Очистить"
     */
    @FXML
    void clear(ActionEvent event) {
        firstOperand.clear();
        secondOperand.clear();
        result.clear();
    }

    /**
     * метод сложения двух вещественных чисел
     * @param event нажатие кнопки "+"
     */
    @FXML
    void add(ActionEvent event) {
        //если не введено число в поле для первого или второго числа,срабатывает анимация
        try {
            animation();
            //ввод в полях чисел, которые нужно сложить
            double first = Integer.parseInt(firstOperand.getText());
            double second = Integer.parseInt(secondOperand.getText());
            double sum = first + second;
            result.setText(String.valueOf(sum));
        } catch (Exception e) {
            //если не введено число в поле для первого или второго числа, в поле выводится сообщение "Введены некорректные данные"
            result.setText(INPUT_ERROR_MESSAGE);
        }
    }

    /**
     * метод деления двух вещественных чисел
     * @param event нажатие кнопки "/"
     */
    @FXML
    void div(ActionEvent event) {
        //если не введено число в поле для первого или второго числа,срабатывает анимация
        try {
            animation();
            //ввод в полях чисел, которые нужно сложить
            double first = Integer.parseInt(firstOperand.getText());
            double second = Integer.parseInt(secondOperand.getText());
            if (second != 0) {
                double quotient = first/second;
                result.setText(String.valueOf(quotient));
            } else {
                //если не введено число в поле для первого или второго числа, в поле выводится сообщение "На ноль делить нельзя"
                result.setText(DIVISION_ERROR);
            }
        } catch (Exception e) {
            //если не введено число в поле для первого или второго числа, в поле выводится сообщение "Введены некорректные данные"

            result.setText(INPUT_ERROR_MESSAGE);
        }
    }

    /**
     * метод умножения двух вещественных чисел
     * @param event нажатие кнопки "*"
     */
    @FXML
    void mult(ActionEvent event) {
        //если не введено число в поле для первого или второго числа,срабатывает анимация
        try {
            animation();
            //ввод в полях чисел, которые нужно сложить
            double first = Integer.parseInt(firstOperand.getText());
            double second = Integer.parseInt(secondOperand.getText());
            double composition = first * second;
            result.setText(String.valueOf(composition));
        } catch (Exception e) {
            //если не введено число в поле для первого или второго числа, в поле выводится сообщение "Введены некорректные данные"

            result.setText(INPUT_ERROR_MESSAGE);
        }
    }

    /**
     * метод вычитания вещественных чисел
     * @param event нажатие кнопки "-"
     */
    @FXML
    void sub(ActionEvent event) {
        //если не введено число в поле для первого или второго числа,срабатывает анимация
        try {
            animation();
            //ввод в полях чисел, которые нужно сложить
            double first = Integer.parseInt(firstOperand.getText());
            double second = Integer.parseInt(secondOperand.getText());
            double difference = first - second;
            result.setText(String.valueOf(difference));
        } catch (Exception e) {
            //если не введено число в поле для первого или второго числа, в поле выводится сообщение "Введены некорректные данные"

            result.setText(INPUT_ERROR_MESSAGE);
        }



    }

    private void animation() {
        if (firstOperand.getText().isEmpty() || secondOperand.getText().isEmpty()) {
            Shake number1Anim = new Shake(firstOperand);
            Shake number2Anim = new Shake(secondOperand);
            number1Anim.playAnimation();
            number2Anim.playAnimation();
            return;
        }
    }

    private void animation(TextField operand) {
        Shake operandAnimation = new Shake(operand);
        operandAnimation.playAnimation();
    }

}